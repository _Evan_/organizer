<?php $this->layout('layout') ?>

<div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?=$task['title'];?></h1>
                <p><?=$task['content'];?>
                </p>
                <a href="/tasks" class="btn btn-primary">Back</a>
            </div>
        </div>
</div>
