<?php
    namespace App;
    class Auth
    {
        public $db;
        public function __construct(QueryBuilder $db)
        {
            $this->db=$db;
        }
        public function register($login,$password)
        {     
            $logins = map($this->db->getAll('users'), function($user){
                return $user['login'];
            });
            
            if(in_array($login,$logins))
                echo "There is the same login";
            else {
                echo "Ok";
                $this->db->store('users', [
                    'login' => $login,
                    'password' => md5($password)
                    ]);

            }
        }
        public function login($login,$password)
        {
           $sql ="SELECT * FROM users WHERE login=:login AND password=:password LIMIT 1";
           $statement = $this->db->pdo->prepare($sql);
           $statement->bindParam(":login",$login);
           $statement->bindParam(":password",md5($password));
           $statement->execute();
           $user = $statement->fetch(PDO::FETCH_ASSOC);
           
           if($user) {
                $_SESSION['user'] = $user;
                return true;
            }
            return false;
        }
        public function logout()
        {
            if(!is_null($_SESSION['user']))
                unset($_SESSION['user']);
        }
        public function check()
        {
            if(isset($_SESSION['user']))
            {
                return true;
            }
            return false;
        }
        public function currentUser()
        {
            if(isset($_SESSION['user']))
            {
                return $_SESSION['user'];
            }
        }
    }
?>

