<?php 
    namespace App;
    use PDO;

    class QueryBuilder {

        public $pdo;

        function __construct($pdo)
        {
            $this->pdo=$pdo;
        }

        function getAll($table){

            $sql = "SELECT * FROM $table";
            $statement = $this->pdo->prepare($sql);
            $statement->execute();
            $result=$statement->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }

        function store($table,$data)
        {    
            $keys = array_keys($data);
            $stringOfKeys = implode(',',$keys);
            $signs = ":".implode(',:',$keys);
            
            $sql = "INSERT INTO $table ($stringOfKeys) VALUES ($signs)";

            $statement = $this->pdo->prepare($sql);
            $statement->execute($data);
        }

        function getOne($table,$id){
            $statement = $this->pdo->prepare("SELECT * FROM $table WHERE Id=:id");
            $statement->bindParam(":id", $id);
            $statement->execute();
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            return $result;
        }
        function update($table,$data){

            $fields = '';
            foreach($data as $key=>$value)
            {
                $fields.=$key."=:".$key.",";
            }
            $fields = rtrim($fields,',');       
            $sql = "UPDATE $table SET $fields WHERE Id=:Id";
            $statement = $this->pdo->prepare($sql);

            $statement->execute($data);    
        }

        function deleteOne($table,$id){
            $sql = "DELETE  FROM $table WHERE Id=:id";
            $statement = $this->pdo->prepare("$sql");
            $statement->bindParam(":id",$id);
            $statement->execute();       
        }
    }
?>