<?php
    class Helper {   
        public static function map($array,$func)
        {
            $result=[];
            foreach($array as $item)
            {
                $result[]=$func($item);
            }
            return $result;
        }
        public static function filter($array,$func)
        {
            $result = [];
            foreach($array as $item)
            {
                if($func($item))
                {
                    $result[]=$item;
                }
            }
        }
}
?>